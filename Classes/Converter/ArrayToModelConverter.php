<?php

declare(strict_types=1);

namespace TWOH\TwohMongodbDriver\Converter;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ArrayToModelConverter
{
    /**
     * @deprecated is not used anymore, because array handling is faster as object handling
     * @param array $array
     * @param $object
     * @return mixed
     */
    public static function mapArray(array $array, $object)
    {
        $class = get_class($object);
        $methods = get_class_methods($class);
        foreach ($methods as $method) {
            preg_match(' /^(set)(.*?)$/i', $method, $results);
            $pre = $results[1]  ?? '';
            $k = $results[2]  ?? '';
            $k = strtolower($k[0]) . substr($k, 1);
            If ($pre === 'set' && !empty($array[$k])) {
                $object->$method($array[$k]);
            }
        }
        return $object;
    }
}